# Message Queues

```sh
$ make with-key MSGKEY=1234
gcc msgqueue.c -o msgqueue -D"MSGKEY=1234"

$ make with-key MSGKEY=5678
gcc msgqueue.c -o msgqueue -D"MSGKEY=5678"
```
